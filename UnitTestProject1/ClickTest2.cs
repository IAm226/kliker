﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using MouseCursor = System.Windows.Forms.Cursor;
using Point = System.Drawing.Point;
using AppForClicking;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClickingTests
{
    [TestClass]
    public class ClickTest2
    {
        [TestMethod]
        public void MouseIntegrityCheck()
        {
            Point CurrentCursorPosition = MouseCursor.Position;
            MouseCursor.Position = new Point(CurrentCursorPosition.X = 0, CurrentCursorPosition.Y = 0);
            
            Assert.AreEqual(0, CurrentCursorPosition.X, 1, "Złe położenie X");
            Assert.AreEqual(0, CurrentCursorPosition.Y, 1, "Złe położenie Y");
        }
    }
}
