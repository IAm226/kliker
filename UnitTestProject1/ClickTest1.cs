﻿using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using System.Timers;
using AppForClicking;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ClickingTests
{
    [TestClass]
    public class ClickTest1
    {
        [TestMethod]
        public void CheckIfTimerWorksProperly() 
        {
            Timer clickTimer = new Timer();
            int Hours=0, Minutes=0, Seconds=2, Milliseconds=500;
            int Interval = Milliseconds + Seconds * 1000 + Minutes * 60 * 1000 + Hours * 60 * 60 * 1000;

            clickTimer.Interval = Interval;

            int expectedValue = 2500;

            Assert.AreEqual(expectedValue, clickTimer.Interval, 1, "Interval not working properly");

        }

    }
}
            


