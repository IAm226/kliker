﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using MouseCursor = System.Windows.Forms.Cursor;
using Point = System.Drawing.Point;

namespace AppForClicking
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
 
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Use of Regions for quick access
        /// </summary>

        /// <summary>
        /// Options region for setting the values and dependancies for Interval and Mouse Clicks
        /// </summary>
        #region Options

        #region Hours
            /// <summary>
            /// Implementation of getter/setter in correspondence with Window "HoursProperty" and setting it to the "value" as a spare setting using WPF.
            /// </summary>
        public int Hours
        {
            get => (int)GetValue(HoursProperty);
            set => SetValue(HoursProperty, value);
        }
        /// <summary>
        /// Returning value during registry of the dependancy and setting it as static element
        /// </summary>
        public static readonly DependencyProperty HoursProperty =
            DependencyProperty.Register(nameof(Hours), typeof(int), typeof(MainWindow),
                new PropertyMetadata(0));

        #endregion Hours

        #region Minutes
        /// <summary>
        /// Same as in Hours, but for minutes.
        /// </summary>
        public int Minutes
        {
            get => (int)GetValue(MinutesProperty);
            set => SetValue(MinutesProperty, value);
        }

        public static readonly DependencyProperty MinutesProperty =
            DependencyProperty.Register(nameof(Minutes), typeof(int), typeof(MainWindow),
                new PropertyMetadata(0));

        #endregion Minutes

        #region Seconds
        /// <summary>
        /// Same as in Hours, but for seconds.
        /// </summary>
        public int Seconds
        {
            get => (int)GetValue(SecondsProperty);
            set => SetValue(SecondsProperty, value);
        }

        public static readonly DependencyProperty SecondsProperty =
            DependencyProperty.Register(nameof(Seconds), typeof(int), typeof(MainWindow),
                new PropertyMetadata(0));

        #endregion Seconds

        #region Milliseconds
        /// <summary>
        /// Same as in Hours, but for miliseconds.
        /// </summary>
        public int Milliseconds
        {
            get => (int)GetValue(MillisecondsProperty);
            set => SetValue(MillisecondsProperty, value);
        }

        public static readonly DependencyProperty MillisecondsProperty =
            DependencyProperty.Register(nameof(Milliseconds), typeof(int), typeof(MainWindow),
                new PropertyMetadata(100));

        #endregion Milliseconds

        #region SelectedMouseButton
        /// <summary>
        /// Same as in Hours, but for Mouse Button selection.
        /// </summary>
        public MouseButton SelectedMouseButton
        {
            get => (MouseButton)GetValue(SelectedMouseButtonProperty);
            set => SetValue(SelectedMouseButtonProperty, value);
        }

        public static readonly DependencyProperty SelectedMouseButtonProperty =
            DependencyProperty.Register(nameof(SelectedMouseButton), typeof(MouseButton), typeof(MainWindow),
                new PropertyMetadata(default(MouseButton)));

        #endregion SelectedMouseButton

        #region SelectedMouseAction
        /// <summary>
        /// Same as in Hours, but for Mouse Action selection.
        /// </summary>
        public MouseAction SelectedMouseAction
        {
            get => (MouseAction)GetValue(SelectedMouseActionProperty);
            set => SetValue(SelectedMouseActionProperty, value);
        }

        public static readonly DependencyProperty SelectedMouseActionProperty =
            DependencyProperty.Register(nameof(SelectedMouseAction), typeof(MouseAction), typeof(MainWindow),
                new PropertyMetadata(default(MouseAction)));


        #endregion SelectedMouseAction

        #endregion Options

        /// <summary>
        /// Configuration of Input Fields. Mouse, Keyboard constants and correspondence between them and script
        /// </summary>
        #region Fields
        /// <summary>
        /// Setting up the Timer in correspondence with Interval during which mouse actions will be taking place.
        /// It is set up with HwndSource which directly interacts between messages from Win32 to WPF.
        /// </summary>
        private readonly Timer clickTimer;
        private int Interval => Milliseconds + Seconds * 1000 + Minutes * 60 * 1000 + Hours * 60 * 60 * 1000;
        private int NumMouseActions => SelectedMouseAction == MouseAction.Pojedyńczo ? 1 : 2;
        private Point CurrentCursorPosition => MouseCursor.Position;

        #region Mouse Constants
        /// <summary>
        /// Constant binded values used for executing mouse actions in functions
        /// </summary>
        private const int MOUSEEVENTF_LEFTDOWN = 0x02;
        private const int MOUSEEVENTF_LEFTUP = 0x04;
        private const int MOUSEEVENTF_RIGHTDOWN = 0x08;
        private const int MOUSEEVENTF_RIGHTUP = 0x10;
        private const int MOUSEEVENTF_MIDDLEDOWN = 0x0020;
        private const int MOUSEEVENTF_MIDDLEUP = 0x0040;

        #endregion Mouse Constants

        #region Keyboard Constants
        /// <summary>
        /// Constant binded values used for executing hotkeys in functions
        /// </summary>
        private const int HOTKEY_ID = 9000;
        private const int WM_HOTKEY = 0x0312;

        private const uint MOD_NONE = 0x0000;
        private const uint F6_KEY = 0x75;
        private const uint F7_KEY = 0x76;

        #endregion Keyboard Constants

        private IntPtr _windowHandle;
        private HwndSource _source;

        #endregion Fields

        /// <summary>
        /// Overall main Initialisation + real time functions
        /// </summary>
        #region Real Time
        /// <summary>
        /// Main window for application in which we initialize all the functions (start program) and handles to work in real time such as Timer
        /// </summary>
        public MainWindow()
        {
            /// <summary>
            /// Start of Real time Timer which will be counting up
            /// </summary>
            clickTimer = new Timer();
            clickTimer.Elapsed += OnClickTimerElapsed;

            DataContext = this; // element for data binding
            InitializeComponent(); // initialization
        }

        /// <summary>
        /// Initialization which happens at the time of program startup in correspondence with Win32 and the app of WPF
        /// Using Override for app to determine the solution
        /// </summary>
        protected override void OnSourceInitialized(EventArgs e)
        {

            base.OnSourceInitialized(e);

            /// <summary>
            /// Creating Hooks to provide user with interaction between Window and himself via Hotkeys
            /// </summary>
            _windowHandle = new WindowInteropHelper(this).Handle;
            _source = HwndSource.FromHwnd(_windowHandle);
            _source.AddHook(StartStopHooks);

            RegisterHotKey(_windowHandle, HOTKEY_ID, MOD_NONE, F6_KEY);
            RegisterHotKey(_windowHandle, HOTKEY_ID, MOD_NONE, F7_KEY);
        }

            /// <summary>
            /// Removal of the hooks and unregistering of hotkeys with application using base for Lifetime interactions
            /// </summary>
        protected override void OnClosed(EventArgs e)
        {
            _source.RemoveHook(StartStopHooks);
            UnregisterHotKey(_windowHandle, HOTKEY_ID);

            base.OnClosed(e);
        }

        #endregion Real Time

        /// <summary>
        /// Set of commands to be used when routed
        /// </summary>
        #region Commands

        /// <summary>
        /// Routed event to execute Start by clicking or pressing Hotkey
        /// Starts Interval clicking and timer
        /// </summary>
        #region Start Command

        private void StartCommand_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            clickTimer.Interval = Interval;
            clickTimer.Start();
        }

        /// <summary>
        /// Check if the timer is enabled to prevent onexpected error
        /// </summary>
        private void StartCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
            => e.CanExecute = !clickTimer.Enabled;


        #endregion Start Command

        /// <summary>
        /// Routed event to Stop timer and setting execution thread to disabled to the clicking stops as well
        /// </summary>

        #region Stop Command

        private void StopCommand_Execute(object sender, ExecutedRoutedEventArgs e)
            => clickTimer.Stop();

        private void StopCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
            => e.CanExecute = clickTimer.Enabled;

        #endregion Stop Command

        #region Exit Command

        /// <summary>
        /// Execution function of closing the application with routed event
        /// </summary>


        private void ExitCommand_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        #endregion Exit Command

        #endregion Commands

        /// <summary>
        /// External methods used to simulate actions
        /// </summary>
        #region External Methods

        /// <summary>
        /// Extrernal methods used for redirecting and enabling usage of Cursor position on screen, mouse events such as clicking and registering hotkeys for custom usage
        /// </summary>
        /// <returns>Cursor position, Mouse flags, Hotkey presets</returns>
        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        static extern bool SetCursorPosition(int x, int y);

        [DllImport("user32.dll", EntryPoint = "mouse_event")]
        static extern void ExecuteMouseEvent(int dwFlags, int dx, int dy, int cButtons, int dwExtraInfo);

        [DllImport("user32.dll")]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        #endregion External Methods

        /// <summary>
        /// Core methods in program eg. Clicking
        /// </summary>
        #region Core Methods

        /// <summary>
        /// Initialisation of a clock Timer to extract events during ticking
        /// </summary>

        private void OnClickTimerElapsed(object sender, ElapsedEventArgs e)
        {
            // Invoking a mouse clicking during certain Interval
            Dispatcher.Invoke(() =>
            {
                InitMouseClick();
            });
        }

        /// <summary>
        /// Mouse clicking function setting with custom button performance depending on the options synchronously
        /// </summary>
        private void InitMouseClick()
        {
            Dispatcher.Invoke(() =>
            {
                // cases for different mouse button options and current position provided with direct communcation between Win32 and Window
                switch (SelectedMouseButton)
                {
                    case MouseButton.Lewy:
                        PerformMouseClick(MOUSEEVENTF_LEFTDOWN, MOUSEEVENTF_LEFTUP, CurrentCursorPosition.X, CurrentCursorPosition.Y);
                        break;
                    case MouseButton.Prawy:
                        PerformMouseClick(MOUSEEVENTF_RIGHTDOWN, MOUSEEVENTF_RIGHTUP, CurrentCursorPosition.X, CurrentCursorPosition.Y);
                        break;
                    case MouseButton.Środkowy:
                        PerformMouseClick(MOUSEEVENTF_MIDDLEDOWN, MOUSEEVENTF_MIDDLEUP, CurrentCursorPosition.X, CurrentCursorPosition.Y);
                        break;
                }
            });
        }

        /// <summary>
        /// Mouse Execution to perform a clicking with values X,Y of mouse position in real time
        /// </summary>

        private void PerformMouseClick(int mouseDownAction, int mouseUpAction, int xPos, int yPos)
        {
            for (int i = 0; i < NumMouseActions; ++i)
            {
                SetCursorPosition(xPos, yPos);
                ExecuteMouseEvent(mouseDownAction | mouseUpAction, xPos, yPos, 0, 0);
            }
        }

        /// <summary>
        /// Hooks which enable communication between Win32 and Window with direct messaging
        /// Enables Hotkeys and presets to activate corellated UI elements on User Side of the program
        /// </summary>
        /// <returns>start, stop or none </returns>
        private IntPtr StartStopHooks(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (msg == WM_HOTKEY && wParam.ToInt32() == HOTKEY_ID) // Checking if hotkey is pressed and setting parameters to hotkey_id
            {
                int vkey = ((int)lParam >> 16) & 0xFFFF; //checking for pressed key to be of certain value
                if (vkey == F6_KEY && !clickTimer.Enabled) // when hotkey is correct and timer is not yet enabled, we dispatch StartCommand_Execute function which enables timer
                {
                    StartCommand_Execute(null, null);
                }
                if (vkey == F7_KEY && clickTimer.Enabled) // when hotkey is correct and timer is enabled, we dispatch StopCommand_Execute function which disabled timer
                {
                    StopCommand_Execute(null, null);
                }
                handled = true;
            }
            return IntPtr.Zero;
        }

        #endregion Core Methods

    }
}
