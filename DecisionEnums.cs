﻿namespace AppForClicking
{
    public enum MouseButton
    {
        Lewy = 0,
        Prawy = 1,
        Środkowy = 2
    }

    public enum MouseAction
    {
        Pojedyńczo = 0,
        Podwójnie = 1
    }
}
